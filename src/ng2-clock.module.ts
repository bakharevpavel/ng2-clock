import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { NgClock } from "./ng2-clock.component";

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [NgClock],
    exports: [NgClock]
})
export class NgClockModule {
}