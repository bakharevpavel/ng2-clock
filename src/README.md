# ng2-clock

Clock/timer component for Angular >= 2.0.0

## Installation

```

npm install --save ng2-clock

```

## Usage

```ts

import { Component } from "angular2/core";
import { Clock } from "./ng2-timer";

@Component({
    selector: "my-app",
    template: `
        <h1>Angular 2 Timer Example</h1>
        <ng-timer [format]="'mm:ss:SSS'"></ng-timer>
    `,
    directives: [TimerComponent]
})
export class AppComponent { }

```
