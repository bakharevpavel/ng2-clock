import { Component, OnInit, Input } from "@angular/core";
import { Observable } from "rxjs/Rx";

@Component({
    selector: "ng-clock",
    template: `{{ time | date: format }}`,
    inputs: ["timestamp", "delay", "format", "interval"]
})
export class NgClock implements OnInit {

    clock: any;

    @Input()
    public time: number = (new Date()).getTime();

    @Input()
    format: string;

    @Input()
    interval: number = 1000;

    @Input()
    delay: number = 0;

    ngOnInit() {
        this.clock = Observable.timer(this.delay, this.interval).subscribe((t) => {
            this.time += this.interval;
        });
    };

    ngOnDestroy() {
        this.clock.unsubscribe();
    };

}